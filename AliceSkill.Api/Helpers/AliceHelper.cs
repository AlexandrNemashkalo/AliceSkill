﻿using AliceSkill.Api.Constants;
using AliceSkill.Api.Models;

namespace AliceSkill.Api.Helpers;

public static class AliceHelper
{
    public static AliceResponse CreateResponse(AliceRequest request, AnswerModel answer)
    {
        AlicePhraseModel alicePhrase = answer.PhraseType == PhrasesEnum.ChatGpt
            ? answer.Message
            : Phrases.GetPhraseText(answer.PhraseType, GetLocale(request.Request.Command));

        return new AliceResponse()
        {
            Response = new AliceResponseModel()
            {
                Text = alicePhrase.Text,
                TTS = GetLocale(alicePhrase.Text) == Locale.Ru ? null : alicePhrase.Text,
                EndSession = false
            },
            SessionState = request.State.Session,
            UserStateUpdate = request.State.User,
            ApplicationState = request.State.Application,
            Version = request.Version,
        };
    }

    private static Locale GetLocale(string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            return Locale.Undefiend;
        }

        var cleanText = text.ToLower().Replace("алиса", "").TrimStart();

        if (cleanText[0] >= 'a' && cleanText[0] <= 'z')
        {
            return Locale.En;
        }

        if (cleanText[0] >= 'а' && cleanText[0] <= 'я')
        {
            return Locale.Ru;
        }

        return Locale.Undefiend;
    }
}
