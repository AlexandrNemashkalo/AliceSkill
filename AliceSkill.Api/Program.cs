﻿namespace AliceSkill.Api;

public static class Program
{
    public static async Task Main(string[] args)
    {
        using var cts = new CancellationTokenSource();

        var host = CreateHostBuilder(args).Build();
        await host.RunAsync();

        Console.ReadLine();
        cts.Cancel();
    }

    private static IHostBuilder CreateHostBuilder(string[] args)
            => Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
}
