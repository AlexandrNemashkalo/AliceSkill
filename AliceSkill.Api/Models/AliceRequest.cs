﻿using Newtonsoft.Json;

namespace AliceSkill.Api.Models;

public class AliceRequest
{
    [JsonProperty("request")]
    public AliceRequestModel Request { get; set; }

    [JsonProperty("session")]
    public AliceRequestSession Session { get; set; }

    [JsonProperty("state")]
    public AliceRequestState State { get; set; }

    [JsonProperty("version")]
    public string Version { get; set; }
}

public class AliceRequestModel
{

    [JsonProperty("command")]
    public string Command { get; set; }

    [JsonProperty("nlu")]
    public AliceRequestNlu Nlu { get; set; }

    [JsonProperty("type")]
    public string Type { get; set; }
}


public class AliceRequestSession
{
    [JsonProperty("message_id")]
    public int MessageId { get; set; }

    [JsonProperty("session_id")]
    public string SessionId { get; set; }

    [JsonProperty("user")]
    public AliceUser User { get; set; }
}

public class AliceUser
{
    [JsonProperty("user_id")]
    public string UserId { get; set; }
}

public class AliceRequestState
{
    [JsonProperty("session")]
    public AliceState Session { get; set; }

    [JsonProperty("user")]
    public AliceState User { get; set; }

    [JsonProperty("application")]
    public AliceState Application { get; set; }
}

public class AliceRequestNlu
{
    [JsonProperty("tokens")]
    public ICollection<string> Tokens { get; set; }

    /*[JsonProperty("intents")]
    public ICollection<AliceRequestIntent> Intents { get; set; }*/
}

public class AliceRequestIntent 
{
    [JsonProperty("type")]
    public string Type { get; set; }

    [JsonProperty("tokens")]
    public AliceRequestIntentToken Tokens { get; set; }

    [JsonProperty("value")]
    public object Value { get; set; }
}

public class AliceRequestIntentToken
{
    [JsonProperty("start")]
    public int Start { get; set; }

    [JsonProperty("end")]
    public int End { get; set; }

}