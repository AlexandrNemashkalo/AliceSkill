﻿namespace AliceSkill.Api.Models;

public class AlicePhraseModel
{
    public AlicePhraseModel(string text)
    {
        Text = text;
    }

    public string Text { get; set; }
    public string TTS { get; set; }

    public static implicit operator AlicePhraseModel(string text) => new AlicePhraseModel(text);
}
