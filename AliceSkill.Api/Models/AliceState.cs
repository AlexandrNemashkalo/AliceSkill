﻿using System.Text.Json.Serialization;

namespace AliceSkill.Api.Models;

public class AliceState
{
    [JsonPropertyName("value")]
    public int Value { get; set; }
}
