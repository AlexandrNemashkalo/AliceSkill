﻿using AliceSkill.Api.Constants;

namespace AliceSkill.Api.Models;

public class AnswerModel
{
    public AnswerModel(string message)
    {
        Message = message;
        PhraseType = PhrasesEnum.ChatGpt;
    }

    public AnswerModel(PhrasesEnum phrase)
    {
        Message = null;
        PhraseType = phrase;
    }

    public string Message { get; }

    public PhrasesEnum PhraseType { get; }

    public static AnswerModel ChatGpt(string message) => new AnswerModel(message);

    public static AnswerModel Phrase(PhrasesEnum phrase) => new AnswerModel(phrase);

}
