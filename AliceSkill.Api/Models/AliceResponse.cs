﻿using Newtonsoft.Json;

namespace AliceSkill.Api.Models;

public class AliceResponse
{
    [JsonProperty("response")]
    public AliceResponseModel Response { get; set; }

    [JsonProperty("session_state")]
    public AliceState SessionState { get; set; }

    [JsonProperty("user_state_update")]
    public AliceState UserStateUpdate { get; set; }

    [JsonProperty("application_state")]
    public AliceState ApplicationState { get; set; }

    [JsonProperty("version")]
    public string Version { get; set; } = "1.0";

}

public class AliceResponseModel
{
    [JsonProperty("text")]
    public string Text { get; set; }

    [JsonProperty("tts")]
    public string TTS { get; set; }

    [JsonProperty("card")]
    public AliceResponseCard Card { get; set; }

    [JsonProperty("buttons")]
    public IEnumerable<AliceResponseButton> Buttons { get; set; }

    [JsonProperty("end_session")]
    public bool EndSession { get; set; }

    [JsonProperty("directives")]
    public object Directives { get; set; }
}


public class AliceResponseCard
{
    [JsonProperty("type")]
    public string Type { get; set; }
}

public class AliceResponseButton
{
    [JsonProperty("title")]
    public string Title { get; set; }

    [JsonProperty("url")]
    public string Url { get; set; }

    [JsonProperty("hide")]
    public bool Hide { get; set; }
}