﻿using AliceSkill.Api.Actions;
using AliceSkill.Api.Data;
using AliceSkill.Api.Services;
using Refit;

namespace AliceSkill.Api;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; private set; }

    public void ConfigureServices(IServiceCollection services)
    {
        services
                .AddControllers()
                .AddNewtonsoftJson(
                x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

        services.AddCors(options =>
        {
            options.AddPolicy("CorsPolicy", policyBuilder => policyBuilder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
            );
        });

        services.AddSingleton<IAction, StartAction>();
        services.AddSingleton<IAction, ResetDialogAction>();
        services.AddSingleton<IAction, AskChatGptAction>();


        services.AddSingleton<IStorage, MongoStorage>();
        services.AddSingleton<MongoContext>();
        services.AddSingleton<ChatGptService>();
        services.AddMemoryCache();

        services.AddRefitClient<IChatGptClient>()
            .ConfigureHttpClient(c => c.BaseAddress = new Uri(Configuration.GetConnectionString("ChatGptApiAddress")));

        services.AddSwagger();
    }

    public void Configure(IApplicationBuilder app)
    {
        app.UseSwagger();
        app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "AliceSkill.Api v1"));

        app.UseHttpsRedirection();
        app.UseRouting();
        app.UseCors("CorsPolicy");
        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }
}