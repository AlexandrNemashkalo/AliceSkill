﻿using AliceSkill.Api.Models;
using AliceSkill.Api.Services;

namespace AliceSkill.Api.Actions;

public class AskChatGptAction : IAction
{
    private readonly ChatGptService _gptService;
    public AskChatGptAction(ChatGptService gptService)
    {
        _gptService = gptService;
    }

    public async Task<AnswerModel> Handle(AliceRequest request)
    {
        return await _gptService.Ask(request.Request.Command, request.Session.User.UserId);
    }

    public bool IsMatch(AliceRequest request)
    {
        return true;
    }
}
