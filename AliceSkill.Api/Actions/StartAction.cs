﻿using AliceSkill.Api.Constants;
using AliceSkill.Api.Models;

namespace AliceSkill.Api.Actions;

public class StartAction : IAction
{
    public async Task<AnswerModel> Handle(AliceRequest request)
    {
        return await Task.FromResult(AnswerModel.Phrase(PhrasesEnum.StartPhrase));
    }

    public bool IsMatch(AliceRequest request)
    {
        return string.IsNullOrEmpty(request.Request.Command);
    }
}