﻿using AliceSkill.Api.Constants;
using AliceSkill.Api.Models;
using AliceSkill.Api.Services;

namespace AliceSkill.Api.Actions;

public class ResetDialogAction : IAction
{
    private readonly ChatGptService _gptService;
    public ResetDialogAction(ChatGptService gptService)
    {
        _gptService = gptService;
    }

    public async Task<AnswerModel> Handle(AliceRequest request)
    {
        return await Task.FromResult(AnswerModel.Phrase(PhrasesEnum.StartPhrase));
    }

    public bool IsMatch(AliceRequest request)
    {
        return request.Request.Nlu.Tokens.Contains("диалог") &&
            ( request.Request.Nlu.Tokens.Contains("сотри")
              || request.Request.Nlu.Tokens.Contains("удали")
              || request.Request.Nlu.Tokens.Contains("очисти")
            );
    }
}
