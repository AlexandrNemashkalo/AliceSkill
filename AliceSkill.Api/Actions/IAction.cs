﻿using AliceSkill.Api.Models;

namespace AliceSkill.Api.Actions;

public interface IAction
{
    public bool IsMatch(AliceRequest request);

    public Task<AnswerModel> Handle(AliceRequest request);
}
