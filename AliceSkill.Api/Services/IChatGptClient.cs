﻿using Refit;

namespace AliceSkill.Api.Services;

public interface IChatGptClient
{
    [Post("/ask")]
    Task<ChatGptMessage> AskAsync([Body] ChatGptMessage message);
}
