﻿using AliceSkill.Api.Constants;
using AliceSkill.Api.Data;
using AliceSkill.Api.Models;
using Microsoft.Extensions.Caching.Memory;

namespace AliceSkill.Api.Services;

public class ChatGptService
{
    private static string GetUserWaitKey(string userId) => "user#wait#" + userId;
    private static string GetUserAnswerKey(string userId) => "user#answer#" + userId;
    private MemoryCacheEntryOptions CacheEntryOptions => new MemoryCacheEntryOptions()
                    .SetAbsoluteExpiration(TimeSpan.FromMinutes(2))
                    .SetPriority(CacheItemPriority.Normal)
                    .SetSize(1024);

    private readonly IChatGptClient _chatGptClient;
    private readonly IMemoryCache _memoryCache;
    private readonly IStorage _data;

    public ChatGptService(IChatGptClient chatGptClient, IMemoryCache memoryCache, IStorage data)
    {
        _chatGptClient = chatGptClient; 
        _memoryCache = memoryCache;
        _data = data;
    }

    public async Task<AnswerModel> Ask(string message, string userId)
    {
        // проверяем есть было ли предыдущее сообщение с ожидаем
        if (_memoryCache.TryGetValue(GetUserWaitKey(userId), out bool isWait))
        {
            // проверяем полили ли мы по предыдущему вопросу ответ
            if (!isWait && _memoryCache.TryGetValue(GetUserAnswerKey(userId), out ChatGptMessage answer))
            {
                // удаляем из кеша данные
                _memoryCache.Remove(GetUserWaitKey(userId));
                _memoryCache.Remove(GetUserAnswerKey(userId));
                return AnswerModel.ChatGpt(answer.Message);
            }

            return AnswerModel.Phrase(PhrasesEnum.INeedMoreTime);
        }

        var dialog = await _data.GetDialogByUserId(userId);

        var request = new ChatGptMessage
        {
            Message = message,
            ConversationId = dialog?.DialogId,
            ParentId = dialog?.ParentId
        };

        // запрашиваем резултат
        var askTask =  _chatGptClient.AskAsync(request);

        // ставим таймаут 
        var isOk = Task.WaitAny(new[] { askTask }, TimeSpan.FromMilliseconds(2800));
        if(isOk == 0)
        {
            var result = await askTask;
            //в случае успеха создаем или обновляем данные в базе (parentId)
            _ = dialog == null
            ? _data.AddUserToDialogs(userId, result.ConversationId, result.ParentId)
            : _data.UpdateDialogParentId(userId, result.ParentId);

            return AnswerModel.ChatGpt(result.Message);
        }
        else
        {
            // если не успели получить результат выставляем флаг ожидания
            _memoryCache.Set(GetUserWaitKey(userId), true, CacheEntryOptions);

            // запускам фоном ожидание
            _ = SaveAskResponseAsync(userId, askTask, dialog);

            return AnswerModel.Phrase(PhrasesEnum.INeedTime);
        }
    }

    public async Task SaveAskResponseAsync(string userId, Task<ChatGptMessage> askTask, UserToDialog dialog)
    {
        // после завершения ожидания мы должны сохранить ответ в память чтобы потом взять его
        // также сохраняем/обновлеям диалог в базе
        var result = await askTask;

        _ = dialog == null
            ? _data.AddUserToDialogs(userId, result.ConversationId, result.ParentId)
            : _data.UpdateDialogParentId(userId, result.ParentId);

        _memoryCache.Set(GetUserAnswerKey(userId), result, CacheEntryOptions);
        _memoryCache.Set(GetUserWaitKey(userId), false, CacheEntryOptions);
    }
}
