﻿using System.Text.Json.Serialization;

namespace AliceSkill.Api.Services;

public class ChatGptMessage
{

    [JsonPropertyName("message")]
    public string Message { get; set; }

    [JsonPropertyName("conversation_id")]
    public string ConversationId { get; set; }

    [JsonPropertyName("parent_id")]
    public string ParentId { get; set; } 
}
