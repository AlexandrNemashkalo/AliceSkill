﻿using AliceSkill.Api.Actions;
using AliceSkill.Api.Constants;
using AliceSkill.Api.Helpers;
using AliceSkill.Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace AliceSkill.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AliceController : ControllerBase
{
    private readonly IEnumerable<IAction> _actions;

    public AliceController(IEnumerable<IAction> actions)
    {
        _actions = actions;
    }

    [HttpPost]
    [Route("/alice")]
    public async Task<IActionResult> Get([FromBody] AliceRequest aliceRequest)
    {
        try
        {
            AnswerModel result = null;
            foreach (var action in _actions)
            {
                if (action.IsMatch(aliceRequest))
                {
                    result = await action.Handle(aliceRequest);
                    break;
                }
            }

            return Ok(AliceHelper.CreateResponse(aliceRequest, result));
        }
        catch
        {
            return Ok(AliceHelper.CreateResponse(aliceRequest, AnswerModel.Phrase(PhrasesEnum.UnknownError)));
        }
        finally
        {

        }
    }
}
