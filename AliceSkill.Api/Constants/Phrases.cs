﻿using AliceSkill.Api.Models;

namespace AliceSkill.Api.Constants;

public static class Phrases
{
    public static AlicePhraseModel GetPhraseText(PhrasesEnum phrase, Locale locale)
    {
        if(locale == Locale.En)
        {
            switch (phrase)
            {
                case PhrasesEnum.StartPhrase: return "";
                case PhrasesEnum.INeedMoreTime: return "I need more time, ask me later";
                case PhrasesEnum.INeedTime: return "I need time, ask me later";
                case PhrasesEnum.UnknownError: return "Sorry, unknown server error";
                default: throw new Exception();
            }
        }
        else
        {
            switch (phrase)
            {
                case PhrasesEnum.StartPhrase: return "";
                case PhrasesEnum.INeedMoreTime: return "Мне нужно еще время, спросите меня позже";
                case PhrasesEnum.INeedTime: return "Мне нужно время, спросите меня позже";
                case PhrasesEnum.UnknownError: return "Извините, неизвестная ошибка на сервере";
                default: throw new Exception();
            }
        }
    }
}
