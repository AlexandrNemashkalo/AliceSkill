﻿namespace AliceSkill.Api.Constants;

public enum PhrasesEnum
{
    ChatGpt = 0,

    INeedTime = 1,

    INeedMoreTime = 2,

    StartPhrase = 3,

    UnknownError = 4
}
