﻿using MongoDB.Bson;
using MongoDB.Driver;

namespace AliceSkill.Api.Data;

public class MongoStorage : IStorage
{
    private readonly MongoContext _context;

    public MongoStorage(MongoContext context)
    {
        _context = context;
    }

    public async Task<UserToDialog> GetDialogByUserId(string userId)
    {
        var result = await _context.UserToDialogs
            .Find(Builders<UserToDialog>.Filter.Eq("userId", userId))
            .FirstOrDefaultAsync();

        return result;
    }

    public async Task AddUserToDialogs(string userId, string dialogId, string parentId)
    {
        var userToDialog = new UserToDialog
        {
            UserId = userId,
            DialogId = dialogId,
            ParentId = parentId
        };

        await _context.UserToDialogs.InsertOneAsync(userToDialog);
    }

    public async Task UpdateDialogParentId(string userId, string parentId)
    {
        var updateSettings = new BsonDocument("$set", new BsonDocument("parentId", parentId));
        await _context.UserToDialogs.UpdateOneAsync(Builders<UserToDialog>.Filter.Eq("userId", userId), updateSettings);
    }
}
