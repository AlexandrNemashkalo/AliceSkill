﻿using MongoDB.Driver;

namespace AliceSkill.Api.Data;

public class MongoContext
{
    private readonly IMongoDatabase _database;

    public MongoContext(IConfiguration config)
    {
        var client = new MongoClient(config.GetConnectionString("MongoConnectionString"));
        _database = client.GetDatabase(config.GetConnectionString("MongoDbName"));
    }

    public IMongoCollection<UserToDialog> UserToDialogs
        => _database.GetCollection<UserToDialog>("UserToDialog");
}
