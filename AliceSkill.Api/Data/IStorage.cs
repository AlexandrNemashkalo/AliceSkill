﻿namespace AliceSkill.Api.Data;

public interface IStorage
{
    public Task<UserToDialog> GetDialogByUserId(string userId);

    public Task AddUserToDialogs(string userId, string dialogId, string parentId);

    public Task UpdateDialogParentId(string userId, string parentId);
}