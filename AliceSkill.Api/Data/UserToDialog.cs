﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace AliceSkill.Api.Data;

public class UserToDialog
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }

    [BsonElement("userId")]
    public string UserId { get; set; }

    [BsonElement("dialogId")]
    public string DialogId { get; set; }

    [BsonElement("parentId")]
    public string ParentId { get; set; }
}
